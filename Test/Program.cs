﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using Masti.QualityAssurance;
using Masti.ImageProcessing;
using Messenger;

namespace Test
{
    class Program
    {
        private static string dateTime = "05/01/1996";
        private static int index = 1;
        static void Main(string[] args)
        {
            if (args[0].Equals("Server"))
            {
                string message = "";
                Console.WriteLine("Enter Server Port");
                int port = 9000;//int.Parse(Console.ReadLine());
                Console.Write("Enter Ip address of Client (Student) :");
                string toIP = "169.254.196.214";//Console.ReadLine();
                IUXMessage uXMessage = new Messager(port);
                uXMessage.SubscribeToStatusReceiver(StatusUpdated);
                uXMessage.SubscribeToDataReceiver(ReceiveMessage);
                uXMessage.SubscribeToConnectifier(ConnectSuccess);
                while (!message.Equals("quit"))
                {
                    message = Console.ReadLine();
                    uXMessage.SendMessage(message, toIP, dateTime);
                }
                IImageProcessing imageProcessing = FactoryImageServer.CreateImageProcessing(port);
                imageProcessing.RegisterImageUpdateHandler(ImageRecivedNotify);
                imageProcessing.RegisterErrorHandler(errorNotify);
                imageProcessing.GetSharedScreen(toIP);
                message = "";
                while (!message.Equals("q"))
                {
                    message = Console.ReadLine();
                    uXMessage.SendMessage(message, toIP, dateTime);
                }
                Console.WriteLine("in program came to stop screen");
                imageProcessing.StopSharedScreen(toIP);
                MastiDiagnostics.WriteLog();
            }
            else if (args[0].Equals("Client"))
            {

                String message = "";
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict["Message"] = "Sample Message";
                Console.WriteLine("Enter Server IPAddress");
                string toIP = "169.254.196.214";//Console.ReadLine();
                Console.WriteLine("Enter Server port");
                int port = 9000;// int.Parse(Console.ReadLine());
                ImageClient imageClient = FactoryImageClient.CreateImageClient(toIP, port);
                IUXMessage uXMessage = new Messager(toIP, port, "User Name");
                uXMessage.SubscribeToStatusReceiver(StatusUpdated);
                uXMessage.SubscribeToDataReceiver(ReceiveMessage);
                uXMessage.SubscribeToConnectifier(ConnectSuccess);
                uXMessage.Connectify("User Name", toIP);
                while (! message.Equals("quit"))
                {
                    message = Console.ReadLine();
                    uXMessage.SendMessage(message, toIP, dateTime);
                }
            }
            
        }

        private static void errorNotify(object sender, ErrorEventArgs e)
        {
            string s = "got the error message to stop with error message " + e.ErrorMessage;
            Console.WriteLine(s);


        }

        private static void ConnectSuccess(string fromIP, string userName)
        {
            Console.WriteLine("Received connection from IP " + fromIP.ToString() + "with user name" + userName);
        }

        private static void ImageRecivedNotify(object sender, ImageEventArgs e)
        {
            Console.WriteLine("recieved image");
            Bitmap bit = e.ImageBitmap;
            bit.Save(index.ToString() + "-ImageRecived.jpeg", ImageFormat.Jpeg);
            index = index + 1;
        }

        private static void ReceiveMessage(string message, string toIP, string fromIP, string dateTime)
        {
            Console.WriteLine("Message Received from Messaging Module of IP " + fromIP.ToString() + ": " + message);
        }

        private static void StatusUpdated(Handler.StatusCode status, string message)
        {
            Console.WriteLine("Message Status for Message : " + message + " : " + status.ToString());
        }
    }
}
