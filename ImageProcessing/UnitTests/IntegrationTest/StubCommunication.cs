﻿// -----------------------------------------------------------------------
// <author> 
//      Siddhardha SST
// </author>
//
// <date> 
//     16-11-2018 
// </date>
// 
// <reviewer>
//      Ravindra Kunar
// </reviewer>
//
// <copyright file="StubCommunication.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file is used to test the Image Processing Module.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.Networking;

    /// <summary>
    /// Delegate signature for receiving notifications on different signals
    /// </summary>
    public delegate void SignalHandlerIntegration();

    /// <summary>
    /// Communicator class implementing ICommunication Interface
    /// </summary>
    public partial class StubCommunication : ICommunication
    {
        /// <summary>
        /// Dictionary which maps Datatype with event handlers (which is triggered on data receival)
        /// </summary>
        private Dictionary<DataType, DataReceivalHandler> dataReceivalEventMap = new Dictionary<DataType, DataReceivalHandler>
        {
            { DataType.ImageSharing, null },
            { DataType.Message, null }
        };

        /// <summary>
        /// Map Signals to Handler functions
        /// </summary>
        private Dictionary<string, SignalHandlerIntegration> imageReceiveEventMap = new Dictionary<string, SignalHandlerIntegration>
        {
            { "Image", null }
        };

        public string LocalIP => throw new NotImplementedException();

        public bool Connected => throw new NotImplementedException();

        /// <summary>
        /// Initializes a new instance of the <see cref="StubCommunication"/> class.
        /// This constructor is used to initialize common data.
        /// </summary>

        public StubCommunication()
        {
            //this.connectedClients = new Hashtable(); // IP , Socket
            //this.acknowledgeStatus = new Hashtable(); // MESSAGEEHASH + IP , AutoResetEvent
            //this.lockStatus = new Hashtable(); // IP , Bool
            //this.lockObjects = new Hashtable(); // IP, Obj
            //this.schema = new ImageSchema();
            //this.queueEvent = new AutoResetEvent(false);
            //this.stopEvent = new ManualResetEvent(true);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is supposed to be used when instantiating the module for a Professor.
        /// </summary>
        /// <param name="port">Port to start the listening server on</param>
        /// <param name="maxRetries">No of times to send a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement in seconds.</param>
        /// <param name="testIP"> For testing purposes.Bind to specific loop back address.(Read Test File For More) </param>
        public StubCommunication(int port, int maxRetries, int waitTimeForAcknowledge, string testIP = "")
        {
            //MastiDiagnostics.LogInfo("Communication Object Created On Professor Machine" + testIP);
            //this.port = port;
            //this.maxRetries = maxRetries;
            //this.waitTimeForAcknowledge = waitTimeForAcknowledge * 1000;
            //this.testIP = testIP;
            //this.Start(); // Strictly Against this, Constructor shouldn't be calling functions
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is supposed to be used when instantiating the module for a Student.
        /// </summary>
        /// <param name="ipAddress">IP address of professor's machine.</param>
        /// <param name="port">Port of professor's machine</param>
        /// <param name="maxRetries">No of times to resend a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement.</param>
        /// <param name="testIP"> For testing purposes.Bind to specific loop back address.(Read Test File For More). </param>
        public StubCommunication(string ipAddress, int port, int maxRetries = 0, int waitTimeForAcknowledge = 0, string testIP = "")
        {
            //MastiDiagnostics.LogInfo("Communication Object Created On Student Machine" + testIP);
            //this.ipAddress = ipAddress;
            //this.isStudent = true;
            //this.port = port;
            //this.maxRetries = maxRetries;
            //this.waitTimeForAcknowledge = waitTimeForAcknowledge * 1000;
            //this.testIP = testIP;
            //this.Start(); // Strictly Against this, Constructor shouldn't be calling functions
        }

        /// <summary>
        /// Method to send data transfer request.
        /// </summary>
        /// <param name="msg">Data to to be send</param>
        /// <param name="targetIP">Recipient IP</param>
        /// <param name="type">Will be used to find component that will notified for message status.</param>
        /// <returns>success status</returns>
        public bool Send(string msg, IPAddress targetIP, DataType type)
        {
            if (msg == "Client Encode Signal Image")
            {
                this.imageReceiveEventMap["Image"]();
            }

            return true;
        }
        
        /// <summary>
        /// Registers Handler to a given Signal type.
        /// Handler will be be called when such a Signal type arrives.
        /// </summary>
        /// <param name="signal">Signal for which notification has to be received</param>
        /// <param name="receivalHandler">Handler that has to be called on signal. This is of signature "void Handler(string data, IPAddress fromIP)"</param>
        /// <returns>Returns 'true' if registering successful. Otherwise returns 'false'.</returns>
        public bool SubscribeForImageReceival(string signal, SignalHandlerIntegration receivalHandler)
        {
            try
            {
                // Protecting Dictionary from concurrent calls
                lock (this.imageReceiveEventMap)
                {
                    this.imageReceiveEventMap[signal] += receivalHandler;
                }

                // MastiDiagnostics.LogSuccess("ImageProcessing: Subscription for " + signal.ToString() + " successful");
                return true;
            }
            catch (KeyNotFoundException)
            {
                // MastiDiagnostics.LogError("ImageProcessing: Subscription to ImageCommunication failed due to invalid key");
                return false;
            }
        }

        /// <summary>
        /// Subscribes the Communication module for receiving messages.
        /// </summary>
        /// <param name="type">type (Message or ImageSharing) of data which should trigger corresponding event handler </param>
        /// <param name="receivalHandler">Event handler which has to be called when data with given type is received</param>
        /// <returns>true on success</returns>
        public bool SubscribeForDataReceival(DataType type, DataReceivalHandler receivalHandler)
        {
            // Protecting Dictionary from concurrent calls
            lock (this.dataReceivalEventMap)
            {
                try
                {
                    this.dataReceivalEventMap[type] += receivalHandler;
                    return true;
                }
                catch (KeyNotFoundException)
                {
                    return false;
                }
            }
        }

        public bool SubscribeForDataStatus(DataType type, DataStatusHandler statusHandler)
        {
            throw new NotImplementedException();
        }

        public void StopCommunication()
        {
            throw new NotImplementedException();
        }
    }
}
