﻿// -----------------------------------------------------------------------
// <author> 
//      Siddhardha SST
// </author>
//
// <date> 
//     16-11-2018 
// </date>
// 
// <reviewer>
//      Ravindra Kumar
// </reviewer>
//
// <copyright file="ImageProcessingClientTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file is used to test the Image Processing Module - Client Side.
// </summary>
// -----------------------------------------------------------------------
namespace Masti.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Net;
    using Masti.Networking;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// This is the base class used for testing Image Processing Module - Server Side.
    /// </summary>
    public class ImageProcessingClientTest : ITest
    {
        /// <summary>
        /// The logger is used for Logging functionality within the test.
        /// Helps in debugging of tests.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Port for client to connect
        /// </summary>
        private readonly int port = 8080;
        
        /// <summary>
        /// Bool variable used in testing like a flag.
        /// </summary>
        private bool status = false;
        
        /// <summary>
        /// List of Clients Ip used for testing
        /// </summary>
        private List<string> clientList = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageProcessingClientTest" /> class.
        /// </summary>
        /// <param name="logger">Assigns the Logger to be used by the Test</param>
        public ImageProcessingClientTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Gets or sets the Communication object for the sending and receiving object
        /// </summary>
        public StubCommunication Communication { get; set; }

        /// <summary>
        /// Gets or sets ImageSchema which is an object to pack and unpack data in proper format.
        /// </summary>
        public ISchema ImageSchema { get; set; }

        /// <summary>
        /// Gets or sets the ImageCompression object for compression and decompression of image
        /// </summary>
        public Compression ImageCompression { get; set; }

        /// <summary>
        /// Gets or sets the ImageReceive class
        /// </summary>
        public ReceiveImage ImageRecieve { get; set; }

        /// <summary>
        /// Gets or sets the Image Communicator class
        /// </summary>
        public ImageCommunication ImageCommunicator { get; set; }

        /// <summary>
        /// Gets or sets the Image Processing Client Class
        /// </summary>
        public ImageProcessingClient ClientImageProcessing { get; set; }
        
        /// <summary>
        /// test Harness Method
        /// </summary>
        /// <returns> returns bool </returns>
        public bool Run()
        {
            if (this.ImageProcessingTestClient())
            {
                this.logger.LogSuccess("SucceSS");
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Main method for Testing
        /// </summary>
        /// <returns> returns bool </returns>
        public bool ImageProcessingTestClient()
        {
            this.logger.LogInfo("Started testing Image Processing Client");
            this.Communication = new StubCommunication(this.port, maxRetries: 1, waitTimeForAcknowledge: 1);
            this.ImageSchema = new StubImageSchema();
            this.ImageCommunicator = new ImageCommunication(this.Communication, this.ImageSchema);
            this.ImageCompression = new Compression();
            this.ClientImageProcessing = new ImageProcessingClient(this.ImageSchema, this.ImageCommunicator, this.ImageCompression);

            this.clientList.Add("10.64.1.1");
            this.clientList.Add("10.64.1.2");
            this.clientList.Add("10.64.1.3");
            this.status = this.Communication.SubscribeForImageReceival("Image", this.StartImageHandler);
            
            if (this.status)
            {
                this.logger.LogSuccess("Subscribed Successfully");
            }
            else
            {
                this.logger.LogError("Subscription Failed");
            }

            IPAddress clientIP = IPAddress.Parse(this.clientList[0]);
            this.ImageCommunicator.ReceiveSignalAndNotify("Server Encode Signal Start", clientIP);
            this.logger.LogInfo("Start Share Event Handler may be Success");
            System.Threading.Thread.Sleep(10000);

            // Console.ReadLine();
            this.ImageCommunicator.ReceiveSignalAndNotify("Server Encode Signal Resend", clientIP);
            this.logger.LogInfo("Resending Share Event Handler may be Success");
            System.Threading.Thread.Sleep(10000);

            // Console.ReadLine();
            this.ImageCommunicator.ReceiveSignalAndNotify("Server Encode Signal Stop", clientIP);
            this.logger.LogInfo("Stop Share Event Handler may be Success");
            System.Threading.Thread.Sleep(10000);

            clientIP = IPAddress.Parse(this.clientList[1]);
            this.ImageCommunicator.ReceiveSignalAndNotify("Server Encode Signal Stop", clientIP);
            this.logger.LogInfo("Stop Share Event Handler may be Success");

            // System.Threading.Thread.Sleep(10000);
            return this.status;
        }

        /// <summary>
        /// Main method for Testing
        /// </summary>
        public void StartImageHandler()
        {
            this.logger.LogSuccess("Received Image");
            this.status = this.status && true;
            return;
        }
    }
}
