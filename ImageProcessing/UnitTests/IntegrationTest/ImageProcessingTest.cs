﻿// -----------------------------------------------------------------------
// <author> 
//      Siddhardha SST
// </author>
//
// <date> 
//     16-11-2018 
// </date>
// 
// <reviewer>
//      Ravindra Kumar
// </reviewer>
//
// <copyright file="ImageProcessingTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file is used to test the Image Processing Module.
// </summary>
// -----------------------------------------------------------------------
namespace Masti.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Net;
    using Masti.Networking;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// This is the base class used for testing Image Processing Module - Server Side.
    /// </summary>
    public class ImageProcessingTest : ITest
    {
        /// <summary>
        /// Port for client to connect
        /// </summary>
        private static int port = 8000;
        
        /// <summary>
        /// The logger is used for Logging functionality within the test.
        /// Helps in debugging of tests.
        /// </summary>
        private readonly ILogger logger;
        
        /// <summary>
        /// Bool variable used in testing like a flag.
        /// </summary>
        private bool status = false;

        /// <summary>
        /// Instance of ImageProcessingObject at Server
        /// </summary>
        private ImageProcessing serverImageProcessing;

        /// <summary>
        /// List of Clients Ip used for testing
        /// </summary>
        private List<string> clientList = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageProcessingTest" /> class.
        /// </summary>
        /// <param name="logger">Assigns the Logger to be used by the Test</param>
        public ImageProcessingTest(ILogger logger)
        {
            this.logger = logger;
            this.logger.LogInfo("Cretaed logger");
        }

        /// <summary>
        /// Gets or sets the Communication object for the sending and receiving object
        /// </summary>
        public ICommunication Communication { get; set; }

        /// <summary>
        /// Gets or sets ImageSchema which is an object to pack and unpack data in proper format.
        /// </summary>
        public ISchema ImageSchema { get; set; }

        /// <summary>
        /// test Harness Method
        /// </summary>
        /// <returns> returns bool </returns>
        public bool Run()
        {
            this.logger.LogInfo("Started Image Processing Test");
            if (this.ImageProcessingTestServer())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Main method for Testing
        /// </summary>
        /// <returns> returns bool </returns>
        public bool ImageProcessingTestServer()
        {
            this.ImageSchema = new StubImageSchema();
            this.Communication = new StubCommunication();
            this.serverImageProcessing = new ImageProcessing(port, this.ImageSchema, this.Communication);

            this.clientList.Add("10.64.1.1");
            this.clientList.Add("10.64.1.2");
            this.clientList.Add("10.64.1.3");
            this.logger.LogInfo("Stared Tetsing Image Processing Server");

            this.status = this.serverImageProcessing.GetSharedScreen(this.clientList[0]);
            System.Threading.Thread.Sleep(10000);
            
            // Check whether Screen Sharing is started
            if (!this.status)
            {
                this.logger.LogError("Failure: Starting Screen Sharing for Client 1 Failed");
                return false;
            }

            this.logger.LogSuccess("ImageProcessing.GetSharedScreen: StartScreenSharing Test Successfull");
            this.status = this.status && this.serverImageProcessing.StopSharedScreen(this.clientList[0]);

            // Check whether Screen Sharing is started
            if (!this.status)
            {
                this.logger.LogError("ImageProcessing.GetSharedScreen: Stoping Screen Sharing for Client 1 Failed");
                return false;
            }

            this.logger.LogSuccess("StopSharedScreen Success");

            // Console.ReadLine();
            this.serverImageProcessing.RegisterImageUpdateHandler(this.ImageUpdateHandler);
            this.serverImageProcessing.RegisterErrorHandler(this.ErrorHandler);
            IPAddress clientIP = IPAddress.Parse(this.clientList[0]);
            this.serverImageProcessing.ImageCommunicator.ReceiveSignalAndNotify("Client Encode Signal Image", clientIP);

            return this.status;
        }
        
        /// <summary>
        /// Image Update Handler is a method that updates the latest image of client to server
        /// </summary>
        /// <param name="source">Source Object</param>
        /// <param name="e">image Event Args</param>
        public void ImageUpdateHandler(object source, ImageEventArgs e)
        {
            this.status = this.status && true;
            this.logger.LogSuccess("Received Image Successfully");
            return;
        }

        /// <summary>
        /// Image Update Handler is a method that updates the latest image of client to server
        /// </summary>
        /// <param name="source">Source Object</param>
        /// <param name="e">error Event Args</param>
        public void ErrorHandler(object source, ErrorEventArgs e)
        {
            this.status = this.status && true;
            this.logger.LogSuccess("Received Error Successfully");
            return;
        }
    }
}
