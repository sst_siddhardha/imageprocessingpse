﻿// -----------------------------------------------------------------------
//  <copyright file="Communication.cs" company="B'15, IIT Palakkad">
//      Open Source. Feel free to use the code, but don't forget to acknowledge. 
//  </copyright>
//  <Module>Netwoking Module</Module>
// <Author>Libin</Author>
// <Author>Parth</Author>
// -----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.Networking;

    /// <summary>
    /// Delegate signature for receiving notifications on different signals
    /// </summary>
    /// <param name="data">Data received</param>
    /// <param name="fromIP">IP from which data has been received</param>
    public delegate void SignalHandlerIntegrat();

    /// <summary>
    /// Communicator class implementing ICommunication Interface
    /// </summary>
    public partial class StubCommunication : ICommunication
    {
        /// <summary>
        /// Map Signals to Handler functions
        /// </summary>
        private Dictionary<string, SignalHandlerIntegrat> imageReceiveEventMap = new Dictionary<string, SignalHandlerIntegrat>
        {
            { "Image", null }
        };

        public string LocalIP => throw new NotImplementedException();

        public bool Connected => throw new NotImplementedException();

        /// <summary>
        /// Method to send data transfer request.
        /// </summary>
        /// <param name="msg">Data to to be send</param>
        /// <param name="targetIP">Recipient IP</param>
        /// <param name="type">Will be used to find component that will notified for message status.</param>
        /// <returns>success status</returns>
        public bool Send(string msg, IPAddress targetIP, DataType type)
        {
            if (msg == "Client Encode Signal Image")
            {
                this.imageReceiveEventMap["Image"]();
            }
            
            return true;
        }

        public void StopCommunication()
        {
            throw new NotImplementedException();
        }

        public bool SubscribeForDataStatus(DataType type, DataStatusHandler statusHandler)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Registers Handler to a given Signal type.
        /// Handler will be be called when such a Signal type arrives.
        /// </summary>
        /// <param name="signal">Signal for which notification has to be received</param>
        /// <param name="receivalHandler">Handler that has to be called on signal. This is of signature "void Handler(string data, IPAddress fromIP)"</param>
        /// <returns>Returns 'true' if registering successful. Otherwise returns 'false'.</returns>
        public bool SubscribeForImageReceival(string signal, SignalHandlerIntegrat receivalHandler)
        {
            try
            {
                // Protecting Dictionary from concurrent calls
                lock (this.imageReceiveEventMap)
                {
                    this.imageReceiveEventMap[signal] += receivalHandler;
                }

                // MastiDiagnostics.LogSuccess("ImageProcessing: Subscription for " + signal.ToString() + " successful");
                return true;
            }
            catch (KeyNotFoundException)
            {
                // MastiDiagnostics.LogError("ImageProcessing: Subscription to ImageCommunication failed due to invalid key");
                return false;
            }
        }
    }
}
