﻿// -----------------------------------------------------------------------
// <author> 
//      Ayush Mittal (29ayush@gmail.com)
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
// Rajat Sharma
// </reviewer>
//
// <copyright file="ClientState.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
//      You are allowed to use the file and/or redistribute/modify as long as you preserve this copyright header and author tag.
// </copyright>
//
// <summary>
//      This class represents the state structure/object used in asynchronous networking calls.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// This class represents the state structure/object used in asynchronous networking calls.
    /// </summary>
    public class ClientState
    {
        /// <summary>
        /// Gets or sets the message to send with additional information (length and reserved Bits)
        /// In byte form.
        /// </summary>
        private byte[] dataToSend;

        /// <summary>
        /// Gets or sets the client socket.
        /// </summary>
        public Socket Socket { get; set; } = null;

        /// <summary>
        /// Gets or sets number of tries already made to send the attached data.
        /// </summary>
        public int Tries { get; set; } = 0;

        /// <summary>
        /// Gets or sets amount Of Data Sent.
        /// </summary>
        public int DataSent { get; set; } = 0;

        /// <summary>
        /// Gets or sets the actual message without any additional information attached.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the IP:Port associated with the client
        /// In which case it's value is null
        /// </summary>
        public IPEndPoint IPPort { get; set; } = null;

        /// <summary>
        /// Defines a function that sets the attribute dataToSend, 
        /// Since properties can't return arrays and, attribute is `get` more than `set`, 
        /// It was better choice to have the `get` accessor as the same name as attribute.
        /// Thus `set` had to be implemented as a function with different name.
        /// </summary>
        /// <param name="input">Input Array</param>
        public void SetDataToSend(byte[] input)
        {
            this.dataToSend = input;
        }

        /// <summary>
        /// Defines a function that sets the attribute dataToSend, 
        /// Since properties can't return arrays, it's `get` accessor had to be implemented as function.
        /// </summary>
        /// <returns>Current Value of dataToSend</returns>
        public byte[] DataToSend()
        {
            return this.dataToSend;
        }
    }
}