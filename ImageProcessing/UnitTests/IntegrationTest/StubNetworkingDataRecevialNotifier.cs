﻿// -----------------------------------------------------------------------
// <author> 
//      Libin N George
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
//      Ayush Mittal
// </reviewer>
//
// <copyright file="DataRecevialNotifier.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file contains Data Receiver Publisher 
//      Publisher - DataReceiveNotifier
//      Function for Subscribing -  SubscribeForDataReceival
//      This file is a part of Networking Module
// </summary>
// -----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using Masti.Networking;
    using Masti.Schema;

    /// <summary>
    /// Notifier for Receiving Data Component of Communication Class
    /// Subscription for receiving messages 
    /// and Triggering the Delegate on receiving message or data 
    /// </summary>
    public partial class StubCommunication : ICommunication
    {
       
        /// <summary>
        /// Dictionary which maps Datatype with event handlers (which is triggered on data receival)
        /// </summary>
        private Dictionary<DataType, DataReceivalHandler> dataReceivalEventMap = new Dictionary<DataType, DataReceivalHandler>
        {
            { DataType.ImageSharing, null },
            { DataType.Message, null }
        };
        
        /// <summary>
        /// Subscribes the Communication module for receiving messages.
        /// </summary>
        /// <param name="type">type (Message or ImageSharing) of data which should trigger corresponding event handler </param>
        /// <param name="receivalHandler">Event handler which has to be called when data with given type is received</param>
        /// <returns>true on success</returns>
        public bool SubscribeForDataReceival(DataType type, DataReceivalHandler receivalHandler)
        {
            // Protecting Dictionary from concurrent calls
            lock (this.dataReceivalEventMap)
            {
                try
                {
                    this.dataReceivalEventMap[type] += receivalHandler;
                    return true;
                }
                catch (KeyNotFoundException)
                {
                    return false;
                }
            }
        }
    }
}
