﻿// -----------------------------------------------------------------------
// <author> 
//      Ayush Mittal (29ayush@gmail.com) 
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
// Rajat Sharma
// </reviewer>
//
// <copyright file="DataOutgoing.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
//      You are allowed to use the file and/or redistribute/modify as long as you preserve this copyright header and author tag.
// </copyright>
//
// <summary>
//      This File contains the Data Outgoing Component.
//      It checks for data in queue by polling.If queue has a message, send it to its designation.
//      This file is a part of Networking Module.
//      Note :: No more changes permitted...... -- As an individual developer it's my right to submit whatever I want,
//      if this file is changed now then it will be considered as privacy intrusion. ----- 
// </summary>
// -----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Collections;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using Masti.Networking;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// Following partial class implements DataOutGoing Component - 
    /// Interface Function Send 
    /// Function to send data from the queue over the network.
    /// Constructors to start this and receiving component.
    /// </summary>
    public partial class StubCommunication : ICommunication 
    {
        /// <summary>
        /// lockObjects,lockStatus,ConnectedClients are HashTables accessed by multiple threads, 
        /// any addition/deletion to these tables is thus made thread safe by using this lock.
        /// </summary>
        private readonly object mainLock = new object();

        /// <summary>
        /// acknowledgeStatus is a HashTable accessed by multiple threads, 
        /// any addition/deletion to this tables is thus made thread safe by using this lock.
        /// </summary>
        private readonly object ackLock = new object();

        /// <summary>
        /// It stores IP of professor and can be accessed by calling the get function.
        /// On professor's machine it's basically the local IP.
        /// </summary>
        private string ipAddress;

        /// <summary>
        /// It stores Port at which professor is listening and can be accessed by calling the get function.
        /// /// </summary>
        private int port;

        /// <summary>
        /// When the module is instantiated as student, it stores whether the socket to professor is working or not.
        /// The purpose it serves is that sometimes the message might fail (due to waitTimeForAcknowledgement being less.) 
        /// But without any fault of network, In that case the module don't need to be restarted,but wait for some time to see
        /// If problem persists.
        /// When the module is instantiated as professor, it stores whether the professor is listening or not, again it helps in debugging purposes.
        /// </summary>
        private bool isRunning = false;

        /// <summary>
        /// Stores the listening socket for professor, nothing for student.
        /// </summary>
        private TcpListener serverSocket;

        /// <summary>
        /// No of times - 1  to `re`send a message if acknowledgement is not received.
        /// </summary>
        private int maxRetries;

        /// <summary>
        /// Time to wait for recipient to send acknowledgement in milliseconds.
        /// </summary>
        private int waitTimeForAcknowledge;

        /// <summary>
        /// Local Variable to Store if this module is instantiated as professor or student.
        /// </summary>
        private bool isStudent = false;

        /// <summary>
        /// Can be used when testing using threads to identify log corresponding to various threads.
        /// </summary>
        private string testIP = string.Empty;

        /// <summary>
        /// Event used to manage wait in SendFromQueue function.
        /// </summary>
        private AutoResetEvent queueEvent;

        /// <summary>
        /// Event used to manage stop the MainThread.
        /// </summary>
        private ManualResetEvent stopEvent;

        /// <summary>
        /// Objective :: Keep a reference to existing connection(socket) corresponding to an IP:Port.
        /// Key       :: IPEndPoint as string.
        /// Value     :: Socket
        /// </summary>
        private Hashtable connectedClients;

        /// <summary>
        /// Objective :: When a message is sent, it is added to this table, It tracks the messages whose acknowledgement is currently not processed.
        /// Key       :: MD5Hash of message + (not hashed)IPEndPoint. 
        /// Value     :: Bool False represents acknowledgement hasn't been received, true represents it has been received.
        /// Note      :: All Messages are supposed to be unique, since schema encodes the timestamps in message.
        /// </summary>
        private Hashtable acknowledgeStatus;

        /// <summary>
        /// Objective :: Since multiple threads are spawned to send messages (Only one queue) over the network with same source, to prevent different threads to send overlapping 
        ///           :: messages locks are used so that only one thread sends the message at a time.
        /// Key       :: IPEndPoint as string. 
        /// Value     :: Object to be used as reference for locks.
        /// </summary>
        private Hashtable lockObjects;

        /// <summary>
        /// Objective :: We use a double checked lock mechanism, it stores true/false check corresponding to the locks.
        /// Key       :: IPEndPoint as string. 
        /// Value     :: Bool to be used as check for locks.
        /// </summary>
        private Hashtable lockStatus;

        /// <summary>
        /// Reference to thread which runs the a function to pop data from queue and process it.
        /// </summary>
        private Thread sendingThread = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is used to initialize common data.
        /// </summary>

        public StubCommunication()
        {
            //this.connectedClients = new Hashtable(); // IP , Socket
            //this.acknowledgeStatus = new Hashtable(); // MESSAGEEHASH + IP , AutoResetEvent
            //this.lockStatus = new Hashtable(); // IP , Bool
            //this.lockObjects = new Hashtable(); // IP, Obj
            //this.schema = new ImageSchema();
            //this.queueEvent = new AutoResetEvent(false);
            //this.stopEvent = new ManualResetEvent(true);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is supposed to be used when instantiating the module for a Professor.
        /// </summary>
        /// <param name="port">Port to start the listening server on</param>
        /// <param name="maxRetries">No of times to send a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement in seconds.</param>
        /// <param name="testIP"> For testing purposes.Bind to specific loop back address.(Read Test File For More) </param>
        public StubCommunication(int port, int maxRetries, int waitTimeForAcknowledge, string testIP = "")
        {
            MastiDiagnostics.LogInfo("Communication Object Created On Professor Machine" + testIP);
            this.port = port;
            this.maxRetries = maxRetries;
            this.waitTimeForAcknowledge = waitTimeForAcknowledge * 1000;
            this.testIP = testIP;
            //this.Start(); // Strictly Against this, Constructor shouldn't be calling functions
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is supposed to be used when instantiating the module for a Student.
        /// </summary>
        /// <param name="ipAddress">IP address of professor's machine.</param>
        /// <param name="port">Port of professor's machine</param>
        /// <param name="maxRetries">No of times to resend a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement.</param>
        /// <param name="testIP"> For testing purposes.Bind to specific loop back address.(Read Test File For More). </param>
        public StubCommunication(string ipAddress, int port, int maxRetries = 0, int waitTimeForAcknowledge = 0, string testIP = "")
        {
            MastiDiagnostics.LogInfo("Communication Object Created On Student Machine" + testIP);
            this.ipAddress = ipAddress;
            this.isStudent = true;
            this.port = port;
            this.maxRetries = maxRetries;
            this.waitTimeForAcknowledge = waitTimeForAcknowledge * 1000;
            this.testIP = testIP;
            //this.Start(); // Strictly Against this, Constructor shouldn't be calling functions
        }

        string ICommunication.LocalIP => throw new NotImplementedException();

        bool ICommunication.Connected => throw new NotImplementedException();

        bool ICommunication.Send(string msg, IPAddress targetIP, DataType type)
        {
            throw new NotImplementedException();
        }

        void ICommunication.StopCommunication()
        {
            throw new NotImplementedException();
        }

        bool ICommunication.SubscribeForDataReceival(DataType type, DataReceivalHandler receivalHandler)
        {
            throw new NotImplementedException();
        }

        bool ICommunication.SubscribeForDataStatus(DataType type, DataStatusHandler statusHandler)
        {
            throw new NotImplementedException();
        }
    }
}