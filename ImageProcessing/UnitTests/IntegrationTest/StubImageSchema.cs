﻿// -----------------------------------------------------------------------
// <author> 
//      Siddhardha SST
// </author>
//
// <date> 
//     16-11-2018 
// </date>
// 
// <reviewer>
//      Ravindra Kumar
// </reviewer>
//
// <copyright file="StubImageSchema.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file is used to test the Image Processing Module.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// Class implementing the ISchema interface which is specific to image encoding and decoding
    /// </summary>
    public class StubImageSchema : ISchema
    {
        /// <summary>
        /// String used to store serializedCompressedImage
        /// </summary>
        private string imageDecode = "null";

        /// <summary>
        /// Decoding the string data into tags in a dictionary
        /// </summary>
        /// <param name="data">encoded data in string format</param>
        /// <param name="partialDecoding">> Boolean if true just returns type of data (ImageProcessing or Messaging)</param>
        /// <returns> Dictionary containing key-value pairs of the encoded data</returns>
        public IDictionary<string, string> Decode(string data, bool partialDecoding)
        {
            Dictionary<string, string> tagDict = new Dictionary<string, string>();
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (data == "Client Encode Image")
            {
                tagDict.Add("implementDiff", "true");
                tagDict.Add("imageDictionary", this.imageDecode);
                return tagDict;
            }

            if (data == "Client Encode Signal Image")
            {
                tagDict.Add("signal","Image");
                tagDict.Add("imageData","Client Encode Image");
                return tagDict;
            }

            if (data == "Server Encode Signal Start")
            {
                tagDict.Add("signal","Start");
                tagDict.Add("imageData","Start Image Signal");
                return tagDict;
            }

            if (data == "Server Encode Signal Stop")
            {
                tagDict.Add("signal", "Stop");
                tagDict.Add("imageData","Stop Image Signal");
                return tagDict;
            }

            if (data == "Server Encode Signal Resend")
            {
                tagDict.Add("signal", "Resend");
                tagDict.Add("imageData", "Resend Image Signal");
                return tagDict;
            }


            //string[] keyValues = data.Split(';');

            // for partial decoding returning the dictionary only containing type of data (ImageProcessing/Messaging)
            //if (partialDecoding == true)
            //{
            //  string[] type = keyValues[0].Split(':');

            // type is the first tag inserted into data while encoding.
            //if (DecodeFrom64(type[0]) == "type")
            //{
            // tagDict.Add(DecodeFrom64(type[0]), DecodeFrom64(type[1]));
            //}
            //else
            //{
            // throw new System.ArgumentException("Data is corrupted as it does not contain the type of data", nameof(data));
            //}

            //return tagDict;
            //}

            // fully decoding if the partialDecoding is false and adding into the dictionary
            //foreach (string keyValue in keyValues)
            //{
            //tagDict.Add(DecodeFrom64(keyValue.Split(':')[0]), DecodeFrom64(keyValue.Split(':')[1]));
            //}

            //tagDict.Remove("type");
            return tagDict;
        }

        /// <summary>
        /// Encodes the data given into a dictionary containing tags into standard format(JSON)
        /// </summary>
        /// <param name="tagDict">Dictionary containing Tags to be encoded</param>
        /// <returns>Encoded data in string format</returns>
        public string Encode(Dictionary<string, string> tagDict)
        {

            if (tagDict == null)
            {
                throw new ArgumentNullException(nameof(tagDict));
            }

            if (tagDict.ContainsKey("implementDiff") && (tagDict["implementDiff"] == "true"))
            {
                //Console.WriteLine("Encode Called by Client");
                if (tagDict.ContainsKey("imageDictionary"))
                {
                    MastiDiagnostics.LogWarning(tagDict["imageDictionary"]);
                    this.imageDecode = tagDict["imageDictionary"];

                }
                return "Client Encode Image";
            }
            else
            {
                if (tagDict.ContainsKey("implementDiff") && (tagDict["implementDiff"] != "true"))
                {
                    //Console.WriteLine("Encode Called by Client");
                    return "Client Encode Image Failed";
                }
            }

            if (tagDict.ContainsKey("signal"))
            {
                if (tagDict["signal"] == "Start")
                {
                    //Console.WriteLine("Encode called by Server");
                    return "Server Encode Signal Start";
                }

                if (tagDict["signal"] == "Stop")
                {
                    //Console.WriteLine("Encode called by Server");
                    return "Server Encode Signal Stop";
                }

                if (tagDict["signal"] == "Resend")
                {
                    //Console.WriteLine("Encode called by Server");
                    return "Server Encode Signal Resend";
                }

                if (tagDict["signal"] == "Image")
                {
                    //Console.WriteLine("Encode called by Client");
                    return "Client Encode Signal Image";
                }
                else
                {
                    //Console.WriteLine("Encode Called Failed");
                    return "Encode Signal Failed";
                }
            }
            else
            {
                return "Error";
            }

            //Dictionary<string, string>.KeyCollection keys = tagDict.Keys;
            //string result = string.Empty;

            // inserting the Type tag into encoding data
            //result = EncodeTo64("type") + ':' + EncodeTo64("ImageProcessing") + ';';
            //foreach (string key in keys)
            //{
            //result += EncodeTo64(key) + ':' + EncodeTo64(tagDict[key]) + ";";
            //}

            //return result.Substring(0, result.Length - 1);
        }

        /// <summary>
        /// Converting string to base64 Encoding
        /// </summary>
        /// <param name="toEncode"> Data to encode into Base64</param>
        /// <returns>Encoded Base64 string</returns>
        //private static string EncodeTo64(string toEncode)
        //{
        //  byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
        //  string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
        //  return returnValue;
        //}

        /// <summary>
        ///  Decode base64 Decoding string
        /// </summary>
        /// <param name="encodedData">Data to decode</param>
        /// <returns>Decoded ASCII string</returns>
        //private static string DecodeFrom64(string encodedData)
        //{
        //  byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
        //  string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
        //  return returnValue;
        //}
    }
}
