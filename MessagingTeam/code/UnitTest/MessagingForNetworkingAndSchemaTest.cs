﻿//-----------------------------------------------------------------------
// <author> 
//    Rahul Dhawan , Sai Nishanth 
// </author>
//
// <date> 
//     11-oct-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj
// </reviewer>
// 
// <copyright file="MessagingForNetworkingAndSchemaTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      The following file contain our SchemaStub.
// </summary>
//-----------------------------------------------------------------------
namespace Masti.Messenger.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.Messenger.Stubs;
    using Masti.QualityAssurance;
    using static Masti.Messenger.Handler;

    /// <summary>
    /// class for implementing ITest
    /// </summary>
    public class MessagingForNetworkingAndSchemaTest : ITest
    {
        /// <summary>
        /// logger object
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// messaging object
        /// </summary>
        private Messager messegerobj;

        /// <summary>
        /// status to check sendMessage function
        /// </summary>
        private bool sendStatus = false;

        /// <summary>
        /// status to check connection
        /// </summary>
        private bool connectStatus = false;

        /// <summary>
        /// status to check subscribing
        /// </summary>
        private bool subscribeToDataReciverStatus = false;

        /// <summary>
        /// status to check subscribing
        /// </summary>
        private bool subscribeToStatusReciverStatus = false;

        /// <summary>
        /// status to check connectifier
        /// </summary>
        private bool connectifierStatus = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagingForNetworkingAndSchemaTest"/> class.
        /// </summary>
        /// <param name="logger">logger parameter</param>
        public MessagingForNetworkingAndSchemaTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// dummy callbacks for data receiving
        /// </summary>
        /// <param name="message">message recieved</param>
        /// <param name="toIP">target ip</param>
        /// <param name="fromIP">source ip</param>
        /// <param name="dateTime">date time</param>
        public void DummyDataRecFun(string message, string toIP, string fromIP, string dateTime)
        {
        }

        /// <summary>
        /// dummy callbacks for status received
        /// </summary>
        /// <param name="status">status of data</param>
        /// <param name="fromIP">source ip</param>
        /// <param name="message">messages sent</param>
        public void DummyStatusRecFun(StatusCode status, string fromIP, string message)
        {
        }

        /// <summary>
        /// dummy callbacks for connection
        /// </summary>
        /// <param name="message">user name</param>
        /// <param name="ip">IP address</param>
        public void DummyConnectifierFun(string message, string ip)
        {
        }

        /// <summary>
        /// runs the test cases
        /// </summary>
        /// <returns>true is test cases pased or false</returns>
        public bool Run()
        {
            this.messegerobj = new Messager(8000)
            {
                Comm = new NetworkingStub(),
                SchemaObj = new SchemaStub()
            };

            this.subscribeToDataReciverStatus = this.messegerobj.SubscribeToDataReceiver(this.DummyDataRecFun);
            this.subscribeToStatusReciverStatus = this.messegerobj.SubscribeToStatusReceiver(this.DummyStatusRecFun);
            this.sendStatus = this.messegerobj.SendMessage("dummyMessage", "127.0.0.1", "2:30");
            this.connectStatus = this.messegerobj.Connectify("dummyName", "127.0.0.1");
            this.connectifierStatus = this.messegerobj.SubscribeToConnectifier(this.DummyConnectifierFun);
            this.messegerobj.StatusCallback("dummyMessage", Masti.Networking.StatusCode.Failure);
            if (this.messegerobj.TestStatusVariable != 2)
            {
                this.logger.LogError("Status Callback failed");
                return false;
            }
            else
            {
                this.logger.LogSuccess("status callback Successfully passed");
            }

            this.messegerobj.TestStatusVariable = 0;
            this.messegerobj.StatusCallback("dummyMessage", Masti.Networking.StatusCode.Success);
            if (this.messegerobj.TestStatusVariable != 1)
            {
                this.logger.LogError("Successfully deleted Messages");
                return false;
            }

            this.messegerobj.TestStatusVariable = 0;
            this.messegerobj.StatusCallback("Message", Masti.Networking.StatusCode.Success);
            if (this.messegerobj.TestStatusVariable != 4)
            {
                return false;
            }

            this.messegerobj.TestStatusVariable = 0;
            this.messegerobj.StatusCallback("Message", Masti.Networking.StatusCode.Failure);
            if (this.messegerobj.TestStatusVariable != 3)
            {
                return false;
            }

            this.messegerobj.DataCallback("Message", IPAddress.Parse("127.0.0.1"));
            if (this.messegerobj.TestDataVariable != 1)
            {
                return false;
            }
            
            this.messegerobj.TestDataVariable = 0;
            this.messegerobj.DataCallback("dummyMessage", IPAddress.Parse("127.0.0.1"));
            if (this.messegerobj.TestDataVariable != 2)
            {
                return false;
            }
           
            if (this.sendStatus == false)
            {
                return false;
            }

            if (this.connectStatus == false)
            {
                return false;
            }

            if (this.subscribeToDataReciverStatus == false)
            {
                return false;
            }

            if (this.subscribeToStatusReciverStatus == false)
            {
                return false;
            }

            if (this.connectifierStatus == false)
            {
                return false;
            }

            return true;
        }
    }
}
